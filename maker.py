from bs4 import BeautifulSoup
import datetime, os, sys, textwrap, time, urllib, urllib2

while True:

  print '-' * 10
  print datetime.datetime.now().strftime('%B %d, %Y - %I:%M %p')
  print '-' * 10

  url = 'http://photography.nationalgeographic.com/photography/photo-of-the-day/'
  urlOpen = urllib2.urlopen(url)

  soup = BeautifulSoup(urlOpen,'html.parser')
  
  for x in soup.find_all('div', class_='primary_photo'):
    imgSource = 'http:' + x.find_all('img')[0]['src']
    try:
      urllib.urlretrieve(imgSource, os.sys.path[0] + '/raw pics/' + datetime.datetime.now().strftime('%Y-%m-%d') + '.' + imgSource[len(imgSource) - 3:])
    except:
      print "Couldn't Save Photo"

  for x in soup.find_all('div', id='caption'):
    title = x.find('h2').string
    print title
    print ''
    photographer = x.find('a').string
    print 'Photographer - ' + photographer
    print ''
    for y in x.find_all('p')[2]:
      caption = y.string
      print textwrap.fill(caption)

  try:
    with open(str(os.sys.path[0] + '/info/' + str(datetime.datetime.now().strftime('%B %d, %Y')) + '.txt'), 'w') as text:
      text.write('Title - ' + title)
      text.write('\n')
      text.write('Photographer - ' + photographer)
      text.write('\n' * 2)
      text.write(caption.encode('ascii','ignore').decode('ascii'))
    text.close()
  except:
    print "Couldn't Save Text"

  time.sleep(1 * 60 * 60 * 24)
