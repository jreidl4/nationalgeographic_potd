Title - Small Fry
Photographer - Eiko Jones

Coho salmon fry peer out of their tank at an elementary school, writes Your Shot member Eiko Jones, who submitted this photo from British Columbia, Canada. In conjunction with the Department of Fisheries, the school raises a tankful of salmon each yearcoho salmon can grow to be up to two feet longand then releases them into a local stream to teach kids about watershed issues and salmon biology.